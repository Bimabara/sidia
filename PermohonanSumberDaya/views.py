from django.shortcuts import render, reverse, redirect
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect


def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def readPermohonanSumberDaya(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to SIDIA")
    cursor.execute("select * from SIDIA.permohonan_sumber_daya")
    hasil = namedtuplefetchall(cursor)
    argument = {
        'hasil' : hasil,
    }
    return render(request, "ReadPermohonanSumberDaya.html", argument)




