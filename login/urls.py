from django.urls import path

from .views import signup, logout_fungsi, masuk

app_name = 'login'

urlpatterns = [
    path('signup/', signup, name="signup"),
    path('logout/', logout_fungsi, name="logout"),
    path('login/', masuk, name="login"),

]