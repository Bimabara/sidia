
# Create your views here.
from django.shortcuts import render, redirect
from .forms import signUpForm, loginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db import IntegrityError
from django.contrib import messages
from django.db import connection
from django.http.response import HttpResponseRedirect

# Create your views here.
def signup(request):
    form = signUpForm()
    if request.method == "POST":
        try:
            form = signUpForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                nama = form.cleaned_data['nama']
                alamat_kel = form.cleaned_data['alamat_kel']
                alamat_kec = form.cleaned_data['alamat_kec']
                alamat_kabkot = form.cleaned_data['alamat_kabkot']
                alamat_prov = form.cleaned_data['alamat_prov']
                no_telepon = form.cleaned_data['no_telepon']
                with connection.cursor() as c:
                    c.execute(f'insert into pengguna values ({username},{password},{nama},{alamat},{alamat_kel},{alamat_kabkot},{alamat_prov},{no_telepon})')
                role = form.cleaned_data['role']
                if role == 'Admin Satgas':
                    c.execute(f'insert into admin_satgas values({username})')
                elif role == 'Petugas Faskes' :
                    c.execute(f'insert into petugas_faskes values({username})')
                elif role == 'Supplier' :
                    c.execute(f'insert into supplier values({username},{nama_organisasi})')
                elif role == 'Petugas Distribusi':
                    c.execute(f'insert into petugas_distribusi values({username},{no_SIM})')
                messages.add_message(request, messages.SUCCESS, "User berhasil dibuat")
                return redirect('/login')
        except IntegrityError:
            messages.add_message(request, messages.ERROR, 'User dengan username tersebut sudah ada')
            return redirect('/signup')
    return render (request, 'login.html', {'form':form, 'nama':'daftar'})

def logout_fungsi(request):
    logout(request)
    return redirect("/")

def masuk (request):
    form = loginForm()
    flag = False
    if request.method == "POST":
        peran = ""
        form = loginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            with connection.cursor() as c:
                c.execute(f"select * from pengguna where username = '{username}' and password = '{password}'" )
                row = c.fetchone()
                if row != None:
                    c.execute(f"select * from admin_satgas where username = '{username}'")
                    admin_satgas = c.fetchone()
                    c.execute(f"select * from petugas_faskes where username = '{username}'")
                    petugas_faskes = c.fetchone()
                    c.execute(f"select * from supplier where username = '{username}'")
                    supplier = c.fetchone()
                    c.execute(f"select * from petugas_distribusi where username = '{username}'")
                    petugas_distribusi = c.fetchone()
                    if admin_satgas:
                        peran = admin_satgas
                    elif petugas_faskes:
                        peran = petugas_faskes
                    elif supplier:
                        peran =supplier
                    else:
                        peran = petugas_distribusi
                    response = {'form':form,'user':user,'flag':flag}
                    return redirect('/')
                else:
                    messages.add_message(request, messages.ERROR, "Incorrect username or password!")
                    return HttpResponseRedirect('/login')
    else:
        response = {'form':form,'flag':flag,'nama':'masuk'}
        return render(request,'login.html',response)

