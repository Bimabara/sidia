--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sidia; Type: SCHEMA; Schema: -; Owner: db202a07
--

CREATE SCHEMA sidia;


ALTER SCHEMA sidia OWNER TO db202a07;

--
-- Name: check_possible_username(); Type: FUNCTION; Schema: sidia; Owner: db202a07
--

CREATE FUNCTION sidia.check_possible_username() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF EXISTS (SELECT * FROM PENGGUNA WHERE username = NEW.username) THEN
RAISE EXCEPTION 'Username % is exist', NEW.username;
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION sidia.check_possible_username() OWNER TO db202a07;

--
-- Name: count_item(); Type: FUNCTION; Schema: sidia; Owner: db202a07
--

CREATE FUNCTION sidia.count_item() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE nama_item VARCHAR(50);
DECLARE jmlh_item_new INT;
DECLARE stock_item INT;
DECLARE cursor record;

BEGIN
 SELECT i.nama INTO nama_item FROM ITEM_SUMBER_DAYA AS i, DAFTAR_ITEM AS d, TRANSAKSI_SUMBER_DAYA AS t
 WHERE NEW.No_transaksi_sumber_daya = t.Nomor AND t.Nomor = d.No_transaksi_sumber_daya AND d.Kode_item_sumber_daya = i.kode;

 SELECT t.Total_item INTO jmlh_item_new FROM TRANSAKSI_SUMBER_DAYA AS t
 WHERE NEW.No_transaksi_sumber_daya = t.Nomor;

 SELECT s.jumlah INTO stock_item FROM PETUGAS_FASKES AS p,FASKES as f, WAREHOUSE_PROVINSI as w, STOK_WAREHOUSE_PROVINSI AS s, Lokasi AS l1, Lokasi AS l2
 WHERE NEW.Username_petugas_faskes = f.Username_petugas AND
 f.Id_lokasi = l1.Id AND l1.Provinsi = l2.Provinsi AND l2.Id = w.Id_lokasi AND
 w.Id_lokasi = s.Id_lokasi_warehouse AND s.Kode_item_sumber_daya = nama_item;

IF (jmlh_item_new > stock_item)
THEN INSERT INTO RIWAYAT_STATUS_PERMOHONAN VALUES('WAI',NEW.no_transaksi_sumber_daya,(SELECT username from ADMIN_SATGAS ORDER BY RANDOM() LIMIT 1),(SELECT CURDATE()));
FOR cursor IN
 SELECT * FROM DAFTAR_ITEM
 WHERE no_transaksi_sumber_daya=NEW.nomor_pesanan
 LOOP
    UPDATE DAFTAR_ITEM SET berat_kumulatif = jumlah_item*(
 select ITD.berat_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 ),
 harga_kumulatif = jumlah_item*(
 select ITD.harga_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 )
  where no_urut=cursor.no_urut;
    END LOOP;

ELSE
INSERT INTO RIWAYAT_STATUS_PERMOHONAN values
('REQ',NEW.no_transaksi_sumber_daya,(SELECT username from ADMIN_SATGAS ORDER BY RANDOM() LIMIT 1),(SELECT CURDATE()));
FOR cursor IN
 SELECT * FROM DAFTAR_ITEM
 WHERE no_transaksi_sumber_daya=NEW.nomor_pesanan
 LOOP
    UPDATE DAFTAR_ITEM SET berat_kumulatif = jumlah_item*(
 select ITD.berat_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 ),
 harga_kumulatif = jumlah_item*(
 select ITD.harga_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 )
  where no_urut=cursor.no_urut;
    END LOOP;


END IF;
RETURN NEW;
END;

$$;


ALTER FUNCTION sidia.count_item() OWNER TO db202a07;

--
-- Name: isi_lokasi_batch(); Type: FUNCTION; Schema: sidia; Owner: db202a07
--

CREATE FUNCTION sidia.isi_lokasi_batch() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
NEW.id_lokasi_asal = (
SELECT wp.id_lokasi from batch_pengiriman bp, transaksi_sumber_daya tsd,
permohonan_sumber_daya_faskes psdf, petugas_faskes pf, faskes f,
warehouse_provinsi wp, lokasi L1, lokasi L2
WHERE bp.nomor_transaksi_sumber_daya = tsd.nomor AND
psdf.no_transaksi_sumber_daya = tsd.nomor AND
psdf.username_petugas_faskes = pf.username AND
f.username_petugas = pf.username AND
f.id_lokasi = L1.id AND
L1.provinsi = L2.provinsi AND
L2.id = wp.id_lokasi
LIMIT 1
);

NEW.id_lokasi_tujuan = (
SELECT f.id_lokasi from batch_pengiriman bp, transaksi_sumber_daya tsd,
permohonan_sumber_daya_faskes psdf, petugas_faskes pf, faskes f
WHERE bp.nomor_transaksi_sumber_daya = tsd.nomor AND
psdf.no_transaksi_sumber_daya = tsd.nomor AND
psdf.username_petugas_faskes = pf.username AND
f.username_petugas = pf.username
LIMIT 1
);

RETURN NEW;
END;
$$;


ALTER FUNCTION sidia.isi_lokasi_batch() OWNER TO db202a07;

--
-- Name: tampilkan_supplier(); Type: FUNCTION; Schema: sidia; Owner: db202a07
--

CREATE FUNCTION sidia.tampilkan_supplier() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
SELECT P.nama as "nama supplier",S.username as "username supplier", ISD.nama as "nama item" from DAFTAR_ITEM as DI, ITEM_SUMBER_DAYA as ISD, SUPPLIER as S, PENGGUNA AS P
WHERE DI.No_transaksi_sumber_daya=NEW.nomor_pesanan and DI.kode_item_sumber_daya=ISD.kode and ISD.username_supplier=S.username and S.username=P.username;
RETURN NEW;
END;
$$;


ALTER FUNCTION sidia.tampilkan_supplier() OWNER TO db202a07;

--
-- Name: ubah_status(); Type: FUNCTION; Schema: sidia; Owner: db202a07
--

CREATE FUNCTION sidia.ubah_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE cursor record;
 BEGIN
 FOR cursor IN
 SELECT * FROM DAFTAR_ITEM
 WHERE no_transaksi_sumber_daya=NEW.nomor_pesanan
 LOOP
    UPDATE DAFTAR_ITEM SET berat_kumulatif = jumlah_item*(
 select ITD.berat_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 ),
 harga_kumulatif = jumlah_item*(
 select ITD.harga_satuan
 from ITEM_SUMBER_DAYA AS ITD
 where ITD.kode=kode_item_sumber_daya
 )
  where no_urut=cursor.no_urut;
    END LOOP;

 INSERT INTO RIWAYAT_STATUS_PESANAN VALUES('REQ-SUP',NEW.nomor_pesanan,(SELECT username_supplier  FROM ITEM_SUMBER_DAYA AS ISD, DAFTAR_ITEM AS DI where ISD.kode=DI.kode_item_sumber_daya and DI.no_transaksi_sumber_daya=NEW.nomor_pesanan LIMIT 1),(SELECT CURDATE()));
 RETURN NEW;
 END;
$$;


ALTER FUNCTION sidia.ubah_status() OWNER TO db202a07;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_satgas; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.admin_satgas (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.admin_satgas OWNER TO db202a07;

--
-- Name: batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.batch_pengiriman (
    kode character varying(5) NOT NULL,
    username_satgas character varying(50) NOT NULL,
    username_petugas_distribusi character varying(50),
    tanda_terima character varying(30) NOT NULL,
    nomor_transaksi_sumber_daya integer NOT NULL,
    id_lokasi_asal character varying(5) NOT NULL,
    id_lokasi_tujuan character varying(5) NOT NULL,
    no_kendaraan character varying(10) NOT NULL
);


ALTER TABLE sidia.batch_pengiriman OWNER TO db202a07;

--
-- Name: daftar_item; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.daftar_item (
    no_transaksi_sumber_daya integer NOT NULL,
    no_urut integer NOT NULL,
    jumlah_item integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    berat_kumulatif integer,
    harga_kumulatif integer
);


ALTER TABLE sidia.daftar_item OWNER TO db202a07;

--
-- Name: faskes; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.faskes (
    kode_faskes_nasional character varying(15) NOT NULL,
    id_lokasi character varying(5) NOT NULL,
    username_petugas character varying(50) NOT NULL,
    kode_tipe_faskes character varying(3) NOT NULL
);


ALTER TABLE sidia.faskes OWNER TO db202a07;

--
-- Name: item_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.item_sumber_daya (
    kode character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    harga_satuan integer NOT NULL,
    berat_satuan integer NOT NULL,
    kode_tipe_item character varying(3) NOT NULL
);


ALTER TABLE sidia.item_sumber_daya OWNER TO db202a07;

--
-- Name: kendaraan; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.kendaraan (
    nomor character varying(10) NOT NULL,
    nama character varying(30) NOT NULL,
    jenis_kendaraan character varying(15) NOT NULL,
    berat_maksimum integer NOT NULL
);


ALTER TABLE sidia.kendaraan OWNER TO db202a07;

--
-- Name: lokasi; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.lokasi (
    id character varying(5) NOT NULL,
    provinsi character varying(20) NOT NULL,
    kabkot character varying(20) NOT NULL,
    kecamatan character varying(20) NOT NULL,
    kelurahan character varying(20) NOT NULL,
    jalan_no character varying(20) NOT NULL
);


ALTER TABLE sidia.lokasi OWNER TO db202a07;

--
-- Name: manifes_item; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.manifes_item (
    kode_batch_pengiriman character varying(5) NOT NULL,
    no_urut integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    jumlah_item integer NOT NULL,
    berat_kumulatif integer
);


ALTER TABLE sidia.manifes_item OWNER TO db202a07;

--
-- Name: pengguna; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat_kel character varying(20) NOT NULL,
    alamat_kec character varying(20) NOT NULL,
    alamat_kabkot character varying(20) NOT NULL,
    alamat_prov character varying(20) NOT NULL,
    no_telp character varying(15) NOT NULL
);


ALTER TABLE sidia.pengguna OWNER TO db202a07;

--
-- Name: permohonan_sumber_daya_faskes; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.permohonan_sumber_daya_faskes (
    no_transaksi_sumber_daya integer NOT NULL,
    username_petugas_faskes character varying(50) NOT NULL,
    catatan text
);


ALTER TABLE sidia.permohonan_sumber_daya_faskes OWNER TO db202a07;

--
-- Name: pesanan_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.pesanan_sumber_daya (
    nomor_pesanan integer NOT NULL,
    username_admin_satgas character varying(50) NOT NULL,
    total_harga integer
);


ALTER TABLE sidia.pesanan_sumber_daya OWNER TO db202a07;

--
-- Name: petugas_distribusi; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.petugas_distribusi (
    username character varying(50) NOT NULL,
    no_sim character varying(15) NOT NULL
);


ALTER TABLE sidia.petugas_distribusi OWNER TO db202a07;

--
-- Name: petugas_faskes; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.petugas_faskes (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.petugas_faskes OWNER TO db202a07;

--
-- Name: riwayat_status_pengiriman; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.riwayat_status_pengiriman (
    kode_status_batch_pengiriman character varying(3) NOT NULL,
    kode_batch character varying(5) NOT NULL,
    tanggal timestamp without time zone NOT NULL
);


ALTER TABLE sidia.riwayat_status_pengiriman OWNER TO db202a07;

--
-- Name: riwayat_status_permohonan; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.riwayat_status_permohonan (
    kode_status_permohonan character varying(3) NOT NULL,
    nomor_permohonan integer NOT NULL,
    username_admin character varying(50) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_permohonan OWNER TO db202a07;

--
-- Name: riwayat_status_pesanan; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.riwayat_status_pesanan (
    kode_status_pesanan character varying(7) NOT NULL,
    no_pesanan integer NOT NULL,
    username_supplier character varying(50) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_pesanan OWNER TO db202a07;

--
-- Name: status_batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.status_batch_pengiriman (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_batch_pengiriman OWNER TO db202a07;

--
-- Name: status_permohonan; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.status_permohonan (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_permohonan OWNER TO db202a07;

--
-- Name: status_pesanan; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.status_pesanan (
    kode character varying(7) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_pesanan OWNER TO db202a07;

--
-- Name: stok_faskes; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.stok_faskes (
    kode_faskes character varying(15) NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    jumlah integer NOT NULL
);


ALTER TABLE sidia.stok_faskes OWNER TO db202a07;

--
-- Name: stok_warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.stok_warehouse_provinsi (
    id_lokasi_warehouse character varying(5) NOT NULL,
    jumlah integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL
);


ALTER TABLE sidia.stok_warehouse_provinsi OWNER TO db202a07;

--
-- Name: supplier; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.supplier (
    username character varying(50) NOT NULL,
    nama_organisasi character varying(20) NOT NULL
);


ALTER TABLE sidia.supplier OWNER TO db202a07;

--
-- Name: tipe_faskes; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.tipe_faskes (
    kode character varying(3) NOT NULL,
    nama_tipe character varying(30) NOT NULL
);


ALTER TABLE sidia.tipe_faskes OWNER TO db202a07;

--
-- Name: tipe_item; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.tipe_item (
    kode character varying(3) NOT NULL,
    nama character varying(50) NOT NULL
);


ALTER TABLE sidia.tipe_item OWNER TO db202a07;

--
-- Name: transaksi_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.transaksi_sumber_daya (
    nomor integer NOT NULL,
    tanggal date NOT NULL,
    total_berat integer,
    total_item integer
);


ALTER TABLE sidia.transaksi_sumber_daya OWNER TO db202a07;

--
-- Name: transaksi_sumber_daya_nomor_seq; Type: SEQUENCE; Schema: sidia; Owner: db202a07
--

CREATE SEQUENCE sidia.transaksi_sumber_daya_nomor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidia.transaksi_sumber_daya_nomor_seq OWNER TO db202a07;

--
-- Name: transaksi_sumber_daya_nomor_seq; Type: SEQUENCE OWNED BY; Schema: sidia; Owner: db202a07
--

ALTER SEQUENCE sidia.transaksi_sumber_daya_nomor_seq OWNED BY sidia.transaksi_sumber_daya.nomor;


--
-- Name: warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.warehouse_provinsi (
    id_lokasi character varying(5) NOT NULL
);


ALTER TABLE sidia.warehouse_provinsi OWNER TO db202a07;

--
-- Name: warehouse_supplier; Type: TABLE; Schema: sidia; Owner: db202a07
--

CREATE TABLE sidia.warehouse_supplier (
    id_lokasi character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL
);


ALTER TABLE sidia.warehouse_supplier OWNER TO db202a07;

--
-- Name: transaksi_sumber_daya nomor; Type: DEFAULT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.transaksi_sumber_daya ALTER COLUMN nomor SET DEFAULT nextval('sidia.transaksi_sumber_daya_nomor_seq'::regclass);


--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.admin_satgas (username) FROM stdin;
aaizikowitza
hpurringtonb
calesic
ctruterd
oconnichiee
gbrissardf
ronealg
\.


--
-- Data for Name: batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.batch_pengiriman (kode, username_satgas, username_petugas_distribusi, tanda_terima, nomor_transaksi_sumber_daya, id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) FROM stdin;
AV001   aaizikowitza    jgeeritzi       Tanda Tangan    102     L0001   L0006   11345.0
AV002   hpurringtonb    pableyj paraf   103     L0002   L0007   11456.0
AV003   calesic ghadlingtonk    Tanda Tangan dan paraf  104     L0003   L0008   22345.0
AV004   gbrissardf      eshortalll      Tanda Tangan    105     L0004   L0009   22456.0
AV005   ronealg msissotm        Tanda Tangan    106     L0005   L0010   33545.0
\.


--
-- Data for Name: daftar_item; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.daftar_item (no_transaksi_sumber_daya, no_urut, jumlah_item, kode_item_sumber_daya, berat_kumulatif, harga_kumulatif) FROM stdin;
101     1       3       k-010   6       30000
102     2       5       k-020   75      2500000
103     3       7       k-030   7       280000
104     4       5       k-040   5       875000
105     5       3       k-050   3       750000
106     6       5       k-060   10      3750000
107     7       4       k-070   8       400000
108     8       3       k-080   6       30000
109     9       7       k-090   105     3500000
110     10      8       k-100   8       320000
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.faskes (kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes) FROM stdin;
10270201.0      L0001   alowdyanen      CN
0112R078        L0010   sklammano       ID
0173R031        L0009   agillan5        RU
26140104.0      L0005   gpenlington6    DK
2209R003        L0002   estopher7       CN
\.


--
-- Data for Name: item_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.item_sumber_daya (kode, username_supplier, nama, harga_satuan, berat_satuan, kode_tipe_item) FROM stdin;
k-010   celwin0 serat sintetis  10000   2       APD
k-020   fshugg1 material akrilik        500000  15      VEN
k-030   ledinborough2   kapuk   40000   1       BED
k-040   mtitterton3     nanofiber       175000  1       MSK
k-050   kbranwhite4     antigen 250000  1       VAK
k-060   celwin0 sampel cairan   750000  2       PCR
k-070   fshugg1 sampel darah    100000  2       RAP
k-080   ledinborough2   serat sintetis  10000   2       APD
k-090   mtitterton3     material akrilik        500000  15      VEN
k-100   kbranwhite4     kapuk   40000   1       BED
k-110   celwin0 nanofiber       175000  1       MSK
k-120   fshugg1 antigen 250000  1       VAK
k-130   ledinborough2   sampel cairan   750000  2       PCR
k-140   mtitterton3     sampel darah    100000  2       RAP
k-141   kbranwhite4     kapuk   40000   1       BED
\.


--
-- Data for Name: kendaraan; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.kendaraan (nomor, nama, jenis_kendaraan, berat_maksimum) FROM stdin;
11345.0 hyundai.        Pesawat.        233000
11456.0 toyota. Mobil.  605
22345.0 daihatsu.       Kapal.  21000
22456.0 honda.  Bus.    24000
33545.0 mitsubishi      Motor.  130
\.


--
-- Data for Name: lokasi; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.lokasi (id, provinsi, kabkot, kecamatan, kelurahan, jalan_no) FROM stdin;
L0001   Jawa Barat      Depok   Beji    Pondok Cina     Jl. Margonda Raya
L0002   Bali    Denpasar        Kesiman Kertalangu      Jl. Raya
L0003   DKI Jakarta     Jakarta Selatan Tebet   Tebet Barat     Jl. Margonda
L0004   Papua Barat     Sorong  Wemak   Kamlin  Jl. Marya
L0005   Papua   Puncak  Mageabume       Anarum  Jl. Maya
L0006   Sulawesi Tengah Tojo    Togean  Popolion        Jl. Gonda Raya
L0007   Aceh    Aceh Singkil    Pidie   Gayo    Jl. Gondaya
L0008   DKI Jakarta     Jakarta Selatan Pasar Minggu    Pasar Minggu    Jl. Harsono, R.M.
L0009   Jawa Tengah     Semarang        Majalengka      Pangandaran     Jl. Harso.
L0010   DKI Jakarta     Jakarta Selatan Kebayoran Baru  Pulo    Jl. R.M. No.125
\.


--
-- Data for Name: manifes_item; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.manifes_item (kode_batch_pengiriman, no_urut, kode_item_sumber_daya, jumlah_item, berat_kumulatif) FROM stdin;
AV001   1       k-010   3       6
AV002   2       k-020   5       75
AV003   3       k-030   7       7
AV004   4       k-040   5       5
AV005   5       k-050   3       3
AV001   6       k-060   5       10
AV002   7       k-070   4       8
AV003   8       k-080   3       6
AV004   9       k-090   7       105
AV005   10      k-100   8       8
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.pengguna (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telp) FROM stdin;
celwin0 saaLyDiu        Caz Elwin       Pukat   Utan    Sumbawa Nusa Tenggara Barat     980-137-3280
fshugg1 uls99K  Franky Shugg    Kerobokan       Kuta Utara      Badung  Bali    576-843-9595
ledinborough2   3pBeDr  Lucina Edinborough      Motong  Utan    Sumbawa Nusa Tenggara Barat     476-796-4368
mtitterton3     9ZRxCN  Marijo Titterton        Kerobokan Kaja  Kuta Utara      Badung  Bali    325-447-1953
kbranwhite4     dvS0251DkfO     Kylie Branwhite Bola    Alok Barat      Sikka   Nusa Tenggara Timur     175-495-5987
agillan5        NFjxMWUE7W4B    Adele Gillan    Hewuli  Alok Barat      Sikka   Nusa Tenggara Timur     954-992-3462
gpenlington6    CUWGolrKh9      Griffith Penlington     Purwosari       Kota Kudus      Kudus   Jawa Tengah     648-601-2971
estopher7       xUaqS3aHQ       Eustacia Stopher        Sebangki        Meranti Landak  Kalimantan Barat        263-642-3428
agabites8       dj1eoHmxdX6     Aryn Gabites    Sukaraja        Ciawi   Bogor   Jawa Barat      629-478-8065
dvan9   ZsX7qJ6s        Dorie Van der Beken     Babakan Ciparay Bandung Jawa Barat      494-390-4318
aaizikowitza    7bqohO7NMRLF    Adrianne Aizikowitz     Air Besar       Meranti Landak  Kalimantan Barat        658-814-8442
hpurringtonb    zq2cEPh5u       Herc Purrington Bojongsari      Bojongsoang     Bandung Jawa Barat      515-775-0424
calesic mWvekTh Cathleen Alesi  Papung  Jelimpo Landak  Kalimantan Barat        123-917-2467
ctruterd        IhtQTLzPzGn     Corinne Truter  Tanjung Batu    Pulau Derawan   Berau   Kalimantan Timur        734-448-5360
oconnichiee     PWBW4Vb6G       Odie Connichie  Lamaru  Balikpapan Timur        Balikpapan      Kalimantan Timur        463-391-6819
gbrissardf      PwaDgJ  Geri Brissard   Pegangsaan Dua  Kelapa Gading   Jakarta Utara   DKI Jakarta     678-168-4393
ronealg aWk2baqg4       Roxi O'Neal     Klender Duren Sawit     Jakarta Timur   DKI Jakarta     449-703-8635
syushmanovh     GtNYUBh4o       Stanislaw Yushmanov     Manggar Balikpapan Timur        Balikpapan      Kalimantan Timur        695-372-9610
jgeeritzi       Etqa2i  Joyann Geeritz  Kelapa Gading Timur     Kelapa Gading   Jakarta Utara   DKI Jakarta     774-761-1913
pableyj FfkOe4mNaB      Petronella Abley        Sunter Jaya     Tanjung Priok   Jakarta Utara   DKI Jakarta     220-802-5029
ghadlingtonk    8DjAGN1tM       Gideon Hadlington       Rawamangun      Pulo Gadung     Jakarta Timur   DKI Jakarta     474-855-6786
eshortalll      H6cWf9U4q       Eloisa Shortall Kayu Putih      Pulo Gadung     Jakarta Timur   DKI Jakarta     204-361-1869
msissotm        Yp7uRs  Mariette Sissot Panjunan        Kota Kudus      Kudus   Jawa Tengah     414-501-0206
alowdyanen      NZdR8UFi        Alexander Lowdyane      Trimulyo        Juwana  Pati    Jawa Tengah     626-966-8382
sklammano       kHqwmQApI675    Stephana Klamman        Duren Sawit     Duren Sawit     Jakarta Timur   DKI Jakarta     994-239-1846
\.


--
-- Data for Name: permohonan_sumber_daya_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.permohonan_sumber_daya_faskes (no_transaksi_sumber_daya, username_petugas_faskes, catatan) FROM stdin;
101     alowdyanen      kirimin PENTING
102     sklammano       persediaan sudah menipis
103     agillan5        NULL
104     gpenlington6    NULL
105     estopher7       kirim lagi
\.


--
-- Data for Name: pesanan_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.pesanan_sumber_daya (nomor_pesanan, username_admin_satgas, total_harga) FROM stdin;
101     calesic 20000
102     ctruterd        30000
103     oconnichiee     40000
104     gbrissardf      50000
105     ronealg 60000
\.


--
-- Data for Name: petugas_distribusi; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.petugas_distribusi (username, no_sim) FROM stdin;
jgeeritzi       80-453-8051
pableyj 61-505-2190
ghadlingtonk    06-325-4207
eshortalll      35-046-7630
msissotm        29-427-6129
\.


--
-- Data for Name: petugas_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.petugas_faskes (username) FROM stdin;
alowdyanen
sklammano
agillan5
gpenlington6
estopher7
\.


--
-- Data for Name: riwayat_status_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.riwayat_status_pengiriman (kode_status_batch_pengiriman, kode_batch, tanggal) FROM stdin;
PRO     AV001   2020-02-06 00:00:00
OTW     AV001   2020-02-10 00:00:00
DLV     AV001   2020-02-17 00:00:00
PRO     AV002   2020-03-05 00:00:00
OTW     AV002   2020-03-10 00:00:00
HLG     AV002   2020-03-21 00:00:00
DLV     AV002   2020-04-16 00:00:00
PRO     AV003   2020-04-04 00:00:00
OTW     AV003   2020-04-08 00:00:00
DLV     AV003   2020-04-16 00:00:00
PRO     AV004   2020-05-03 00:00:00
OTW     AV004   2020-05-05 00:00:00
DLV     AV004   2020-05-14 00:00:00
PRO     AV005   2020-06-02 00:00:00
OTW     AV005   2020-06-03 00:00:00
RET     AV005   2020-06-10 00:00:00
DLV     AV005   2020-07-10 00:00:00
RET     AV004   2020-06-10 00:00:00
RET     AV003   2020-07-10 00:00:00
RET     AV001   2020-07-10 00:00:00
\.


--
-- Data for Name: riwayat_status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.riwayat_status_permohonan (kode_status_permohonan, nomor_permohonan, username_admin, tanggal) FROM stdin;
REQ     101     aaizikowitza    2019-02-06
PRO     102     hpurringtonb    2019-06-10
WAI     103     calesic 2019-08-13
FIN     104     ctruterd        2019-09-05
REJ     105     oconnichiee     2019-09-05
MAS     101     gbrissardf      2019-09-07
REQ     102     ronealg 2019-09-08
PRO     103     oconnichiee     2019-10-09
WAI     104     gbrissardf      2019-10-25
FIN     105     ronealg 2019-12-29
\.


--
-- Data for Name: riwayat_status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.riwayat_status_pesanan (kode_status_pesanan, no_pesanan, username_supplier, tanggal) FROM stdin;
REQ-SUP 101     celwin0 2020-04-30
REQ-SUP 102     fshugg1 2020-04-16
REQ-SUP 103     ledinborough2   2020-08-04
REQ-SUP 104     mtitterton3     2020-06-21
REQ-SUP 105     kbranwhite4     2020-12-13
PRO-SUP 101     fshugg1 2020-01-14
FIN-SUP 102     ledinborough2   2020-04-04
MAS-SUP 105     mtitterton3     2020-09-29
PRO-SUP 104     kbranwhite4     2020-02-28
FIN-SUP 103     ledinborough2   2020-04-04
\.


--
-- Data for Name: status_batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.status_batch_pengiriman (kode, nama) FROM stdin;
PRO     Diproses
OTW     Dalam perjalanan
DLV     Selesai
HLG     Hilang
RET     Dikembalikan
\.


--
-- Data for Name: status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.status_permohonan (kode, nama) FROM stdin;
REQ     Diajukan
PRO     Diproses
WAI     Menunggu Pengadaan
FIN     Selesai
REJ     Ditolak
MAS     Selesai dengan Masalah
\.


--
-- Data for Name: status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.status_pesanan (kode, nama) FROM stdin;
REQ-SUP Diajukan
REJ-SUP Ditolak
PRO-SUP Diproses
FIN-SUP Selesai
MAS-SUP Selesai dengan masalah
\.


--
-- Data for Name: stok_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.stok_faskes (kode_faskes, kode_item_sumber_daya, jumlah) FROM stdin;
10270201.0      k-010   4
0112R078        k-020   5
0173R031        k-030   7
26140104.0      k-040   5
2209R003        k-050   3
10270201.0      k-060   4
0112R078        k-070   2
0173R031        k-080   6
26140104.0      k-090   5
2209R003        k-100   4
10270201.0      k-110   7
0112R078        k-120   8
0173R031        k-130   7
26140104.0      k-140   8
2209R003        k-141   5
\.


--
-- Data for Name: stok_warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.stok_warehouse_provinsi (id_lokasi_warehouse, jumlah, kode_item_sumber_daya) FROM stdin;
L0010   4       k-010
L0005   5       k-020
L0001   3       k-030
L0009   6       k-040
L0002   5       k-050
L0010   7       k-060
L0005   2       k-070
L0001   6       k-080
L0009   5       k-090
L0002   6       k-100
L0010   8       k-110
L0005   4       k-120
L0001   3       k-130
L0009   6       k-140
L0001   7       k-141
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.supplier (username, nama_organisasi) FROM stdin;
celwin0 Linklinks
fshugg1 Zoombeat
ledinborough2   Fivechat
mtitterton3     Mynte
kbranwhite4     Nlounge
\.


--
-- Data for Name: tipe_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.tipe_faskes (kode, nama_tipe) FROM stdin;
CN      Rumah sakit rujukan
ID      Instalasi darurat terpadu
RU      Klinik
DK      Puskesman
\.


--
-- Data for Name: tipe_item; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.tipe_item (kode, nama) FROM stdin;
APD     alat pelindung diri
VEN     ventilator
BED     tempat tidur
MSK     paket masker
VAK     paket vaksin
PCR     PCR test kit
RAP     rapid test kit
\.


--
-- Data for Name: transaksi_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.transaksi_sumber_daya (nomor, tanggal, total_berat, total_item) FROM stdin;
101     2020-04-30      6       3
102     2020-05-16      75      5
103     2020-06-04      7       7
104     2020-06-05      5       5
105     2020-06-13      3       3
106     2020-06-17      10      5
107     2020-06-26      8       4
108     2020-08-09      6       3
109     2020-08-27      105     7
110     2020-10-03      8       8
\.


--
-- Data for Name: warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.warehouse_provinsi (id_lokasi) FROM stdin;
L0010
L0005
L0001
L0009
L0002
\.


--
-- Data for Name: warehouse_supplier; Type: TABLE DATA; Schema: sidia; Owner: db202a07
--

COPY sidia.warehouse_supplier (id_lokasi, username_supplier) FROM stdin;
L0010   celwin0
L0005   fshugg1
L0001   ledinborough2
L0009   mtitterton3
L0002   kbranwhite4
\.


--
-- Name: transaksi_sumber_daya_nomor_seq; Type: SEQUENCE SET; Schema: sidia; Owner: db202a07
--

SELECT pg_catalog.setval('sidia.transaksi_sumber_daya_nomor_seq', 1, false);


--
-- Name: admin_satgas admin_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: batch_pengiriman batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: daftar_item daftar_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_pkey PRIMARY KEY (no_transaksi_sumber_daya, no_urut);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode_faskes_nasional);


--
-- Name: item_sumber_daya item_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_pkey PRIMARY KEY (kode);


--
-- Name: kendaraan kendaraan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.kendaraan
    ADD CONSTRAINT kendaraan_pkey PRIMARY KEY (nomor);


--
-- Name: lokasi lokasi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.lokasi
    ADD CONSTRAINT lokasi_pkey PRIMARY KEY (id);


--
-- Name: manifes_item manifes_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_pkey PRIMARY KEY (kode_batch_pengiriman, no_urut);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (username);


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_pkey PRIMARY KEY (no_transaksi_sumber_daya);


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_pkey PRIMARY KEY (nomor_pesanan);


--
-- Name: petugas_distribusi petugas_distribusi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_pkey PRIMARY KEY (username);


--
-- Name: petugas_faskes petugas_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_pkey PRIMARY KEY (username);


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_pkey PRIMARY KEY (kode_status_batch_pengiriman, kode_batch);


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_pkey PRIMARY KEY (kode_status_permohonan, nomor_permohonan);


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_pkey PRIMARY KEY (kode_status_pesanan, no_pesanan);


--
-- Name: status_batch_pengiriman status_batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.status_batch_pengiriman
    ADD CONSTRAINT status_batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: status_permohonan status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.status_permohonan
    ADD CONSTRAINT status_permohonan_pkey PRIMARY KEY (kode);


--
-- Name: status_pesanan status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.status_pesanan
    ADD CONSTRAINT status_pesanan_pkey PRIMARY KEY (kode);


--
-- Name: stok_faskes stok_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_pkey PRIMARY KEY (kode_faskes, kode_item_sumber_daya);


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_pkey PRIMARY KEY (id_lokasi_warehouse, kode_item_sumber_daya);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (username);


--
-- Name: tipe_faskes tipe_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.tipe_faskes
    ADD CONSTRAINT tipe_faskes_pkey PRIMARY KEY (kode);


--
-- Name: tipe_item tipe_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.tipe_item
    ADD CONSTRAINT tipe_item_pkey PRIMARY KEY (kode);


--
-- Name: transaksi_sumber_daya transaksi_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.transaksi_sumber_daya
    ADD CONSTRAINT transaksi_sumber_daya_pkey PRIMARY KEY (nomor);


--
-- Name: warehouse_provinsi warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_pkey PRIMARY KEY (id_lokasi);


--
-- Name: warehouse_supplier warehouse_supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_pkey PRIMARY KEY (id_lokasi);


--
-- Name: permohonan_sumber_daya_faskes check_item; Type: TRIGGER; Schema: sidia; Owner: db202a07
--

CREATE TRIGGER check_item BEFORE INSERT ON sidia.permohonan_sumber_daya_faskes FOR EACH ROW EXECUTE PROCEDURE sidia.count_item();


--
-- Name: pengguna check_possible_username; Type: TRIGGER; Schema: sidia; Owner: db202a07
--

CREATE TRIGGER check_possible_username BEFORE INSERT OR UPDATE OF username ON sidia.pengguna FOR EACH ROW EXECUTE PROCEDURE sidia.check_possible_username();


--
-- Name: batch_pengiriman isi_lokasi_batch; Type: TRIGGER; Schema: sidia; Owner: db202a07
--

CREATE TRIGGER isi_lokasi_batch BEFORE INSERT ON sidia.batch_pengiriman FOR EACH ROW EXECUTE PROCEDURE sidia.isi_lokasi_batch();


--
-- Name: pesanan_sumber_daya memeriksa_dan_menampilkan_supplier; Type: TRIGGER; Schema: sidia; Owner: db202a07
--

CREATE TRIGGER memeriksa_dan_menampilkan_supplier BEFORE INSERT ON sidia.pesanan_sumber_daya FOR EACH STATEMENT EXECUTE PROCEDURE sidia.tampilkan_supplier();


--
-- Name: pesanan_sumber_daya ubah_riwayat_status_pesanan; Type: TRIGGER; Schema: sidia; Owner: db202a07
--

CREATE TRIGGER ubah_riwayat_status_pesanan AFTER INSERT ON sidia.pesanan_sumber_daya FOR EACH STATEMENT EXECUTE PROCEDURE sidia.ubah_status();


--
-- Name: admin_satgas admin_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_asal_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_asal_fkey FOREIGN KEY (id_lokasi_asal) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_tujuan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_tujuan_fkey FOREIGN KEY (id_lokasi_tujuan) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_no_kendaraan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_no_kendaraan_fkey FOREIGN KEY (no_kendaraan) REFERENCES sidia.kendaraan(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_nomor_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_nomor_transaksi_sumber_daya_fkey FOREIGN KEY (nomor_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_username_petugas_distribusi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_petugas_distribusi_fkey FOREIGN KEY (username_petugas_distribusi) REFERENCES sidia.petugas_distribusi(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_username_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_satgas_fkey FOREIGN KEY (username_satgas) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_item daftar_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_item daftar_item_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_kode_tipe_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_kode_tipe_faskes_fkey FOREIGN KEY (kode_tipe_faskes) REFERENCES sidia.tipe_faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_username_petugas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_username_petugas_fkey FOREIGN KEY (username_petugas) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_kode_tipe_item_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_kode_tipe_item_fkey FOREIGN KEY (kode_tipe_item) REFERENCES sidia.tipe_item(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: manifes_item manifes_item_kode_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_batch_pengiriman_fkey FOREIGN KEY (kode_batch_pengiriman) REFERENCES sidia.batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: manifes_item manifes_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_username_petugas_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_username_petugas_faskes_fkey FOREIGN KEY (username_petugas_faskes) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_nomor_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_nomor_pesanan_fkey FOREIGN KEY (nomor_pesanan) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_username_admin_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_username_admin_satgas_fkey FOREIGN KEY (username_admin_satgas) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_distribusi petugas_distribusi_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_faskes petugas_faskes_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_batch_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_batch_fkey FOREIGN KEY (kode_batch) REFERENCES sidia.batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey FOREIGN KEY (kode_status_batch_pengiriman) REFERENCES sidia.status_batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_kode_status_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_kode_status_permohonan_fkey FOREIGN KEY (kode_status_permohonan) REFERENCES sidia.status_permohonan(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_nomor_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_nomor_permohonan_fkey FOREIGN KEY (nomor_permohonan) REFERENCES sidia.permohonan_sumber_daya_faskes(no_transaksi_sumber_daya) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_username_admin_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_username_admin_fkey FOREIGN KEY (username_admin) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_kode_status_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_kode_status_pesanan_fkey FOREIGN KEY (kode_status_pesanan) REFERENCES sidia.status_pesanan(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_no_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_no_pesanan_fkey FOREIGN KEY (no_pesanan) REFERENCES sidia.pesanan_sumber_daya(nomor_pesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES sidia.faskes(kode_faskes_nasional) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_id_lokasi_warehouse_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_id_lokasi_warehouse_fkey FOREIGN KEY (id_lokasi_warehouse) REFERENCES sidia.warehouse_provinsi(id_lokasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier supplier_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_provinsi warehouse_provinsi_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202a07
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--
