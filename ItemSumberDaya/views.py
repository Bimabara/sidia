from django.shortcuts import render, reverse, redirect
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect


def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def readItemSumberDaya(request):
    cursor = connection.cursor()
    cursor.execute("SET search_path TO SIDIA")
    cursor.execute("select * from SIDIA.item_sumber_daya")
    hasil = namedtuplefetchall(cursor)
    argument = {
        'hasil' : hasil,
    }
    return render(request, "ReadItemSumberDaya.html", argument)

def deleteItemSumberDaya(request, kode):
    kode = str(kode)
    print(kode)
    cursor = connection.cursor()
    cursor.execute("set search_path to sidia")
    cursor.execute("delete from sidia.item_sumber_daya where kode = '"+kode+"'")
    return redirect('/ItemSumberDaya/')
