from django.contrib import admin
from django.urls import path
from .views import *


urlpatterns = [
     path('ItemSumberDaya/', readItemSumberDaya, name='ReadItemSumberDaya'),
     path('ItemSumberDaya/delete/<str:kode>/', deleteItemSumberDaya, name='deleteItemSumberDaya'),
]